$(document).ready(function() {

	var questions_count = $(".question-area").data("count");

	function setAnswer($field, answer) {
		$field.val(answer);
	}

	function addAnswer($field, answer) {
		var multiple_answers = [];
		var value = $field.val();

		if(value) {
			multiple_answers = value.split(",");
		}
		if(multiple_answers.indexOf(answer) == -1) {
			multiple_answers.push(answer);
		}
		$field.val(multiple_answers);
	}

	function deleteAnswer($field) {
		$field.val("");
	}

	function removeAnswer($field, answer) {
		var value = $field.val();
		multiple_answers = value.split(",");

		var index = multiple_answers.indexOf(answer);
		if(index > -1) {
			multiple_answers.splice(index, 1);
		}

		$field.val(multiple_answers);
	}

	$(".tile").click(function() {
		var is_multiple = $(this).parent().parent().hasClass("question-multiple");
		var multiple_answers = [];
		var count = -1;
		var limit = 0;

		if(!is_multiple) {
			if(!$(this).hasClass("selected")) {
				$(this).parent().parent().find(".tile").removeClass("selected");
			}
			$(".next-question").click();
		} else {
			count = $(this).parent().parent().find(".tile.selected").length;
			limit = $(this).parent().parent().data("limit");
		}

		if(count < limit || $(this).hasClass("selected")) {
			$(this).toggleClass("selected");

			var answer = $(this).data("value");
			var $field = $(this).parent().parent().find(".answer");

			if($(this).hasClass("selected")) {
				if(!is_multiple) {
					setAnswer($field, answer);
				} else {
					addAnswer($field, answer);
				}
			} else {
				if(!is_multiple) {
					deleteAnswer();
				} else {
					removeAnswer($field, answer);
				}
			}
		}

		if(count < limit && is_multiple) {
			count = $(this).parent().parent().find(".tile.selected").length;
			limit = $(this).parent().parent().data("limit");
			if(count == limit) {
				$(".next-question").click();
			}
		}

	});

	function updateProgressPanel(current_id) {
		$(".questions-progress > i").each(function() {
			var qid = $(this).data("qid");

			if(qid < current_id) {
				$(this).addClass("fa-circle").removeClass("fa-circle-o fa-times-circle fa-spin");
			} else if(qid == current_id) {
				$(this).addClass("fa-times-circle-o fa-spin").removeClass("fa-circle fa-circle-o");
			} else {
				$(this).addClass("fa-circle-o").removeClass("fa-circle fa-times-circle-o fa-spin");
			}
		});
	}

	$(".questions-progress > i").click(function() {
		var qid = $(this).data("qid");

		$(".question:not(.question-hidden)").addClass("question-hidden");
		$(".question[data-qid=" + qid + "]").removeClass("question-hidden");

		if(qid == 1) {
			$(".previous-question").addClass("hidden");
			$(".next-question").removeClass("hidden");
			$(".submit-questions").addClass("hidden");
		} else if(qid == questions_count) {
			$(".next-question").addClass("hidden");
			$(".previous-question").removeClass("hidden");
			$(".submit-questions").removeClass("hidden");
		} else {
			$(".next-question, .previous-question").removeClass("hidden");
			$(".submit-questions").addClass("hidden");
		}

		updateProgressPanel(qid);
	});

	$(".next-question").click(function() {
		var qid = $(this).parent().parent().find(".question:not(.question-hidden)").data("qid");

		$(".question[data-qid=" + qid + "]").addClass("question-hidden");
		$(".question[data-qid=" + (qid + 1) + "]").removeClass("question-hidden");

		if(qid == 1) {
			$(".previous-question").removeClass("hidden");
		}

		if(qid == questions_count - 1) {
			$(".next-question").addClass("hidden");
			$(".submit-questions").removeClass("hidden");
		}

		updateProgressPanel(qid + 1);
	});

	$(".previous-question").click(function() {
		var qid = $(this).parent().parent().find(".question:not(.question-hidden)").data("qid");

		$(".question[data-qid=" + qid + "]").addClass("question-hidden");
		$(".question[data-qid=" + (qid - 1) + "]").removeClass("question-hidden");

		if(qid == 2) {
			$(".previous-question").addClass("hidden");
		}

		if(qid == questions_count) {
			$(".next-question").removeClass("hidden");
			$(".submit-questions").addClass("hidden");
		}

		updateProgressPanel(qid - 1);
	});

	function getResultData(category_ids) {
		var url = $("#preloader").data("allegro");

		$.ajax({
			url: url,
			type: "post",
			data: {
				"_token": $("#_token").val(),
				"category_ids": category_ids,
				"keywords": $("#keywords").val()
			},
			success: function(data) {
				$("#result").append(data);
				$("#result").show(500);
				$("#preloader").parent().hide(500);
			}
		});
	}

	function checkIfOutFileHasBeenGenerated() {
		$.ajax({
			url: $("#preloader").data("checkfile"),
			type: "post",
			data: {
				"_token": $("#_token").val(),
			},
			dataType: "json",
			success: function(e) {
				if(e.success) {
					getResultData(e.category_id);
				} else {
					console.log("File not found, wait 2.5 seconds...");
					setTimeout(function() {
						checkIfOutFileHasBeenGenerated();
					}, 2500);
				}
			}
		});
	}

	if($("#preloader").length) {
		checkIfOutFileHasBeenGenerated();
	}

});
