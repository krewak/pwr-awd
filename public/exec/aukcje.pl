
:- consult('odpowiedzi.pl').
:- consult('mozliwosci.pl').
:- consult('mapowanie.pl').
start :-
	czyscOdpowiedzi,
	wszystkieMozliwosci.

wszystkieMozliwosci :-
	findall(X, mozliwosc(X), Odp),
	writeln(Odp),
	zapis(Odp).

:- dynamic(odpowiedzUzytkownika/2).

czyscOdpowiedzi :- retract(odpowiedzUzytkownika(_)),fail.
czyscOdpowiedzi.

odpowiedz(Q,A) :- odpowiedzUzytkownika(Q,A).

odpowiedz(Q,A) :- \+ odpowiedzUzytkownika(Q,_),
	odczytajOdpowiedz(Q,Odp),
%	asserta(odpowiedzUzytkownika(_,Odp)),
	A = Odp.

intersects([H|_],List) :-
    member(H,List),
    !.
intersects([_|T],List) :-
    intersects(T,List).
    
zapis(Odpowiedzi) :-
	open('C:/wamp/www/pwr/awd/storage/app/out.txt', write, Stream),
	print_all(Odpowiedzi, Stream),
	close(Stream).

print_all([], Stream) :- write(Stream, "11697").
print_all([X|Rest], Stream) :- mapujKategorie(X,Map), write(Stream,Map), write(Stream, '\n'), print_all(Rest, Stream).