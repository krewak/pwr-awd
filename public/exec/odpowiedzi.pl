odczytajOdpowiedz(Stream,Odp) :-
	read_string(Stream, "\n", "\r\t ", End, String),
	atomic_list_concat(L,' ', String),
	findall(X, (member(Y,L), interpret(Y, X)), Odp),
	writeln(Odp),
                         !.                      % cut -- see text


odczytajOdpowiedz(Odp) :- nl,
                         odczytajOdpowiedz(Odp).

interpret("tak",tak).
interpret("Tak",tak).
interpret("nie",nie).
interpret("Nie",nie).
interpret("kobieta", kobieta).
interpret("mezczyzna", mezczyzna).
interpret("dziecko", dziecko).
interpret("nastolatek", nastolatek).
interpret("osoba_dorosla", os_dorosla).
interpret("osoba_starsza", os_starsza).
interpret("partner", partner).
interpret("rodzina", rodzina).
interpret("kolega", kolega).
interpret("dalszy_znajomy", dalszy_znajomy).
interpret("drobna_pamiatka", drobna_pamiatka).
interpret("przydatny_lub_uzyteczny", przydatny).
interpret("drogi", drogi).
interpret("fotografia", fotografia).
interpret("gotowanie", gotowanie).
interpret("gry", gry).
interpret("jezyki", jezyki).
interpret("kino", kino).
interpret("kolekcjonerstwo", kolekcjonerstwo).
interpret("literatura", literatura).
interpret("moda", moda).
interpret("muzyka", muzyka).
interpret("nauka", nauka).
interpret("ogrodnictwo", ogrodnictwo).
interpret("podroze", podroze).
interpret("sport", sport).
interpret("sporty_ekstremalne", sporty_ekstremalne).
interpret("sztuka", sztuka).
interpret("technologie", technologie).
interpret("wedkarstwo/lowiectwo", wedkarstwo_lowiectwo).
interpret("inne", inne).
interpret("bystry", bystry).
interpret("domator", domator).
interpret("energiczny", energiczny).
interpret("gadatliwy", gadatliwy).
interpret("gadzeciarz", gadzeciarz).
interpret("len", len).
interpret("majsterkowicz", majsterkowicz).
interpret("odludek", odludek).
interpret("otwarty", otwarty).
interpret("sentymentalny", sentymentalny).
interpret("spoznialski", spoznialski).
interpret("towarzyski", towarzyski).
interpret("wyksztalcony", wyksztalcony).
interpret("zabawny", zabawny).
interpret("lasuch", lasuch).
interpret("zywy", zywy).
interpret("alkohol", alkohol).
interpret("herbata", herbata).
interpret("kawa", kawa).
interpret("tyton", tyton).
interpret("", brak).

interpret('tak',tak).
interpret('Tak',tak).
interpret('nie',nie).
interpret('Nie',nie).
interpret('kobieta', kobieta).
interpret('mezczyzna', mezczyzna).
interpret('dziecko', dziecko).
interpret('nastolatek', nastolatek).
interpret('osoba_dorosla', os_dorosla).
interpret('osoba_starsza', os_starsza).
interpret('partner', partner).
interpret('rodzina', rodzina).
interpret('kolega', kolega).
interpret('dalszy_znajomy', dalszy_znajomy).
interpret('drobna_pamiatka', drobna_pamiatka).
interpret('przydatny_lub_uzyteczny', przydatny).
interpret('drogi', drogi).
interpret('fotografia', fotografia).
interpret('gotowanie', gotowanie).
interpret('gry', gry).
interpret('jezyki', jezyki).
interpret('kino', kino).
interpret('kolekcjonerstwo', kolekcjonerstwo).
interpret('literatura', literatura).
interpret('moda', moda).
interpret('muzyka', muzyka).
interpret('nauka', nauka).
interpret('ogrodnictwo', ogrodnictwo).
interpret('podroze', podroze).
interpret('sport', sport).
interpret('sporty_ekstremalne', sporty_ekstremalne).
interpret('sztuka', sztuka).
interpret('technologie', technologie).
interpret('wedkarstwo/lowiectwo', wedkarstwo_lowiectwo).
interpret('inne', inne).
interpret('bystry', bystry).
interpret('domator', domator).
interpret('energiczny', energiczny).
interpret('gadatliwy', gadatliwy).
interpret('gadzeciarz', gadzeciarz).
interpret('len', len).
interpret('majsterkowicz', majsterkowicz).
interpret('odludek', odludek).
interpret('otwarty', otwarty).
interpret('sentymentalny', sentymentalny).
interpret('spoznialski', spoznialski).
interpret('towarzyski', towarzyski).
interpret('wyksztalcony', wyksztalcony).
interpret('zabawny', zabawny).
interpret('lasuch', lasuch).
interpret('zywy', zywy).
interpret('alkohol', alkohol).
interpret('herbata', herbata).
interpret('kawa', kawa).
interpret('tyton', tyton).
interpret('', brak).
