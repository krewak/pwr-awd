# Opis projektu #
Zadanie projektowe polegało na zaproponowaniu problemu związanego z algorytmami wspomagania decyzji oraz na wykonaniu i przedstawieniu jego fizycznej implementacji. 

Zdecydowano się na stworzenie systemu decyzyjnego, którego zadaniem miało być wspomaganie użytkownika w trudnej sytuacji wyboru prezentu dla bliskiej osoby. Aby nie bazować na suchych danych, za cel postawiono zapoznanie się oraz implementację Allegro API, aby móc pobierać rzeczywiste informacje z największej platformy transakcyjnej on-line w Polsce.

## Schemat działania ##
Schemat działania aplikacji można przedstawić w uproszczonej formie następująco:
* generowana jest ankieta uprzednio zaprojektowana przez autorów
* użytkownik odpowiada na pytania i wysyła odpowiedzi na serwer
* serwer przetwarza odpowiedzi, zapisuje je w pliku, uruchamia skrypt systemu decyzyjnego i generuje ekran oczekiwania na wyniki
* system decyzyjny stara się wskazać kategorię przedmiotów, która najbardziej pasuje do przedstawionego profilu użytkownika
* co określony czas sprawdzane jest czy system decyzyjny wygenerował już odpowiedzi
* gdy odpowiedzi zostaną wygenerowane, uruchamiane jest połączenie przez Allegro API w celu wyłuskania wyników
* wyniki przedstawiane są użytkownikowi w sposób dynamiczny

![awd.png](https://bitbucket.org/repo/qqdeny/images/1622152540-awd.png)

![awd2.png](https://bitbucket.org/repo/qqdeny/images/3176817652-awd2.png)

![awd3.png](https://bitbucket.org/repo/qqdeny/images/3329483851-awd3.png)