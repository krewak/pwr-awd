<?php

Route::get("/", ["as" => "home", "uses" => "HomeController@getHome"]);

Route::post("/", ["as" => "post", "uses" => "HomeController@postAnswers"]);
Route::post("/checkfile", ["as" => "checkfile", "uses" => "DataController@checkIfOutFileHasBeenGenerated"]);
Route::post("/allegro", ["as" => "allegro", "uses" => "DataController@getAllegro"]);

