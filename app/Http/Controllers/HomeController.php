<?php namespace App\Http\Controllers;

use App\Models\Question;
use Html, Storage, Request;

class HomeController extends Controller {

	protected $polishSigns = ["Ą", "Ć", "Ę", "Ł", "Ń", "Ó", "Ś", "Ź", "Ż", "ą", "ć", "ę", "ł", "ń", "ó", "ś", "ź", "ż", " ", ","];
	protected $signs = ["A", "C", "E", "L", "N", "O", "S", "Z", "Z", "a", "c", "e", "l", "n", "o", "s", "z", "z", "_", " "];

	public function getHome() {
		return view("home", [
			"questions" => $this->getQuestions()
		]);
	}

	public function postAnswers() {
		$filename = "in.txt";
		Storage::put($filename, "");

		foreach(Request::except("_token") as $key => $value) {
			if(in_array($key, ["hobbies", "traits"])) {
				if(!strpos($value, "inne")) {
					$value .= ",inne";
				}
			}

			Storage::append($filename, $this->prepareValue($value));
		}

		$this->runProlog();

		return view("result", [
			"keywords" => Request::get("keywords")
		]);
	}

	protected function runProlog() {
		$file_path = public_path() ."\\exec\\aukcje.pl";
		$cmd = 'C:\\progra~2\\swipl\\bin\\swipl.exe -s '. $file_path .' -g "start." -t halt';

		shell_exec($cmd);
	}

	protected function getQuestions() {
		$questions = [];

		$question = new Question("sex");
		$question->setQuestion("Jakiej płci jest obdarowywana osoba?");
		$question->setAsSexChoice();
		array_push($questions, $question);

		$question = new Question("age");
		$question->setQuestion("W jakim jest wieku?");
		$question->setAsSingleChoice();
		$question->setDefaultIcon("user");
		$question->addAnswer("dziecko");
		$question->addAnswer("nastolatek");
		$question->addAnswer("osoba dorosła");
		$question->addAnswer("osoba starsza");
		array_push($questions, $question);

		$question = new Question("relation");
		$question->setQuestion("Jaka jest Wasza relacja?");
		$question->setAsSingleChoice();
		$question->setDefaultIcon("users");
		$question->addAnswer("partner");
		$question->addAnswer("rodzina");
		$question->addAnswer("kolega");
		$question->addAnswer("dalszy znajomy");
		array_push($questions, $question);

		$question = new Question("type");
		$question->setQuestion("Jaki to ma być rodzaj prezentu?");
		$question->setAsSingleChoice();
		$question->setDefaultIcon("gift");
		$question->addAnswer("drobna pamiątka");
		$question->addAnswer("przydatny lub użyteczny");
		$question->addAnswer("drogi");
		array_push($questions, $question);

		$question = new Question("hobbies");
		$question->setQuestion("Wskaż maksymalnie 3 zainteresowania rzeczonej osoby.");
		$question->setAsMultipleChoice(3);
		$question->addAnswer("fotografia", "camera");
		$question->addAnswer("gotowanie", "cutlery");
		$question->addAnswer("gry", "gamepad");
		$question->addAnswer("języki", "globe");
		$question->addAnswer("kino", "video-camera");
		$question->addAnswer("kolekcjonerstwo", "cubes");
		$question->addAnswer("literatura", "book");
		$question->addAnswer("moda", "black-tie");
		$question->addAnswer("muzyka", "music");
		$question->addAnswer("nauka", "flask");
		$question->addAnswer("ogrodnictwo", "pagelines");
		$question->addAnswer("podróże", "briefcase");
		$question->addAnswer("sport", "futbol-o");
		$question->addAnswer("sporty ekstremalne", "trophy");
		$question->addAnswer("sztuka", "paint-brush");
		$question->addAnswer("technologie", "rocket");
		$question->addAnswer("wędkarstwo/łowiectwo", "anchor");
		$question->addAnswer("inne", "star");
		array_push($questions, $question);

		$question = new Question("traits");
		$question->setQuestion("Wskaż maksymalnie 3 cechy rzeczonej osoby.");
		$question->setAsMultipleChoice(3);
		$question->setDefaultIcon("user");
		$question->addAnswer("bystry");
		$question->addAnswer("domator");
		$question->addAnswer("energiczny");
		$question->addAnswer("gadatliwy");
		$question->addAnswer("gadżeciarz");
		$question->addAnswer("leń");
		$question->addAnswer("majsterkowicz");
		$question->addAnswer("odludek");
		$question->addAnswer("otwarty");
		$question->addAnswer("sentymentalny");
		$question->addAnswer("spóźnialski");
		$question->addAnswer("towarzyski");
		$question->addAnswer("wykształcony");
		$question->addAnswer("zabawny");
		$question->addAnswer("łasuch");
		$question->addAnswer("żywy");
		array_push($questions, $question);

		$question = new Question("stimulants");
		$question->setQuestion("Czy rzeczona osoba lubi jakies używki?");
		$question->setAsMultipleChoice(4);
		$question->setDefaultIcon("users");
		$question->addAnswer("alkohol", "glass");
		$question->addAnswer("herbata", "coffee");
		$question->addAnswer("kawa", "coffee");
		$question->addAnswer("tytoń", "fire");
		array_push($questions, $question);

		$question = new Question("hobbyist");
		$question->setQuestion("Czy rzeczona osoba jest hobbystą?");
		$question->setAsBooleanChoice();
		array_push($questions, $question);

		$question = new Question("animal");
		$question->setQuestion("Czy rzeczona osoba ma zwierzątko domowe?");
		$question->setAsBooleanChoice();
		array_push($questions, $question);

		$question = new Question("keywords");
		$question->setQuestion("Podaj dodatkowe słowa kluczowe, aby zawęzić obszar poszukiwań.");
		$question->setAsOpenField();
		array_push($questions, $question);

		return $questions;
	}

	protected function prepareValue($string) {
		return str_replace($this->polishSigns, $this->signs, $string);
	}

}
