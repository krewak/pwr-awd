<?php namespace App\Http\Controllers;

use Artisaninweb\SoapWrapper\Facades\SoapWrapper;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Exception, Storage, Request, Response;

const POLAND_COUNTRY_ID = 1;
const CATEGORIES_LIMIT = 5;
const RESULTS_LIMIT = 100;

class DataController extends Controller {

	public function checkIfOutFileHasBeenGenerated() {
		$data = ["success" => true];

		try {
			$data["category_id"] = explode("\r\n", Storage::get("out.txt"));
			Storage::delete(["in.txt", "out.txt"]);
		} catch (Exception $e) {
			$data = ["success" => false];
		}

		return Response::json($data);
	}

	public function getAllegro() {
		$searched_string = Request::get("searched_string");

		$category_ids = Request::get("category_ids");
		shuffle($category_ids);
		$category_ids = array_slice($category_ids, 0, CATEGORIES_LIMIT);

		$categories_count = count(Request::get("category_ids"));
		$results_batch_limit = (int) ceil(RESULTS_LIMIT/$categories_count);

		$query = null;

		foreach($category_ids as $category_id) {
			$subquery = $this->getSoapWrapper($category_id, $searched_string);

			if(!$subquery->itemsCount) {
				$subquery = $this->getSoapWrapper($category_id);
			} else if($subquery->itemsCount < $results_batch_limit) {
				$additional_query = $this->getSoapWrapper($category_id);
				$subquery->itemsList->item = array_merge($subquery->itemsList->item, array_slice($additional_query->itemsList->item, 0, ($results_batch_limit - $subquery->itemsCount - 1)));
				$subquery->itemsCount += $additional_query->itemsCount;
			}

			$subquery->itemsList->item = array_slice($subquery->itemsList->item, 0, $results_batch_limit);

			if(is_null($query)) { 
				$query = $subquery;
			} else {
				$query->itemsList->item = array_merge($query->itemsList->item, $subquery->itemsList->item);
				$query->categoriesList->categoriesTree->item = array_merge($query->categoriesList->categoriesTree->item, $subquery->categoriesList->categoriesTree->item);
				$query->itemsCount += $subquery->itemsCount;
			}
		}

		shuffle($query->itemsList->item);

		return view("query", [
			"items" => $query->itemsList->item,
			"categories" => $query->categoriesList->categoriesTree->item,
			"conditions" => [
				"new" => "nowy",
				"used" => "używany",
				"undefined" => "b/d",
			],
			"price_types" => [
				"buyNow" => "kup teraz",
				"bidding" => "licytuj",
				"withDelivery" => "z dostawą",
			],
		]);
	}

	protected function getSoapWrapper($category_id, $searched_string = null) {
		SoapWrapper::override(function($service) {
			$service->name("Allegro")->wsdl("https://webapi.allegro.pl/service.php?wsdl");
		});

		$query_filters = $this->getQueryFilters($category_id, $searched_string);

		return SoapWrapper::service("Allegro", function($service) {})->call("doGetItemsList", $query_filters);
	}

	protected function getQueryFilters($category_id, $searched_string) {
		$query_filters = [[]];

		$query_filters[0]["webapiKey"] = getenv("WEBAPI_KEY");
		$query_filters[0]["countryId"] = POLAND_COUNTRY_ID;
		$query_filters[0]["filterOptions"] = [];
		$query_filters[0]["sortOptions"] = [
			"sortType" => "popularity",
		];

		if($searched_string) {
			array_push($query_filters[0]["filterOptions"], [
				"filterId" => "search",
				"filterValueId" => [$searched_string],
			]);
		}

		array_push($query_filters[0]["filterOptions"], [
			"filterId" => "category",
			"filterValueId" => [$category_id],
		]);

		return $query_filters;
	}

}
