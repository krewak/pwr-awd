<?php namespace App\Http\Controllers;

use Auth, Redirect, Session;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller {

	public function postLogin(LoginRequest $request) {
		$userdata = array(
			"username" => $request->get("username"),
			"password" => $request->get("password")
		);
		if(Auth::attempt($userdata, !is_null($request->get("remember")))) {
			Session::flash("success", "Poprawnie zalogowano do systemu."); 
		} else {
			Session::flash("error", "Nie zalogowano do systemu. Wprowadzono błędny login lub hasło."); 
		}
		return Redirect::route("home");
	}

	public function getLogout() {
		Auth::logout();
		Session::flash("success", "Wylogowano poprawnie z systemu."); 
		return Redirect::route("home");
	}

}
