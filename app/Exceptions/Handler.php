<?php namespace App\Exceptions;

use Exception, Session;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	public function report(Exception $e)
	{
		return parent::report($e);
	}

	public function render($request, Exception $e)
	{
		if($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
			Session::flash('error', 'Błąd 404. Nastąpiło przekierowanie na skutek niedozwolonej operacji.');
			return redirect(route("index"));
		}

		return parent::render($request, $e);
	}

}
