<?php 

namespace App\Services;

use Carbon\Carbon;

class ExtendedCarbon {

	public static function getMonth($month) {
		$months = [
			1 => "stycznia",
			2 => "lutego",
			3 => "marca",
			4 => "kwietnia",
			5 => "maja",
			6 => "czerwca",
			7 => "lipca",
			8 => "sierpnia",
			9 => "września",
			10 => "października",
			11 => "listopada",
			12 => "grudnia",
		];
		return $months[$month];
	}

	public static function getProperWeekForm($number) {
		$abs = abs($number);
		if($abs == 1) {
			return "tydzień";
		} else if($abs == 2) {
			return "dwa tygodnie";
		} else if(($abs == 3 || $abs == 4) || ($abs%10 == 2 || $abs%10 == 3 || $abs%10 == 4) && !($abs == 12 || $abs == 13 || $abs == 14)) {
			return $abs ." tygodnie";
		} else {
			return $abs ." tygodni";
		}
	}

	public static function getProperMonthForm($number) {
		$abs = abs($number);
		if($abs == 1) {
			return "miesiąc";
		} else if($abs == 2) {
			return "dwa miesiące";
		} else if(($abs == 3 || $abs == 4) || ($abs%10 == 2 || $abs%10 == 3 || $abs%10 == 4) && !($abs == 12 || $abs == 13 || $abs == 14)) {
			return $abs ." miesięce";
		} else {
			return $abs ." miesięcy";
		}
	}

	public static function getProperYearForm($number) {
		$abs = abs($number);
		if($abs == 1) {
			return "rok";
		} else if($abs == 2) {
			return "dwa lata";
		} else if(($abs == 3 || $abs == 4) || ($abs%10 == 2 || $abs%10 == 3 || $abs%10 == 4) && !($abs == 12 || $abs == 13 || $abs == 14)) {
			return $abs ." lata";
		} else {
			return $abs ." lat";
		}
	}

	public static function format(Carbon $date) {
		return $date->format("d") ." ". self::getMonth($date->format("n")) ." ". $date->format("Y");
	}

	public static function interpret(Carbon $date) {
		if($date->isToday()) {
			return "dzisiaj";
		}

		if($date->isTomorrow()) {
			return "jutro";
		}

		if($date->isYesterday()) {
			return "wczoraj";
		}

		$days = Carbon::now()->diffInDays($date, false);
		if(abs($days) < 14) {
			if($days < 0) {
				return abs($days) ." dni temu";
			} else {
				return "za ". abs($days) ." dni";
			}
		}
		
		$weeks = Carbon::now()->diffInWeeks($date, false);
		if(abs($days) < 56) {
			if($weeks < 0) {
				return self::getProperWeekForm($weeks) ." temu";
			} else {
				return "za ". self::getProperWeekForm($weeks);
			}
		}
		
		$months = Carbon::now()->diffInMonths($date, false);
		if(abs($days) < 365) {
			if($months < 0) {
				return self::getProperMonthForm($months) ." temu";
			} else {
				return "za ". self::getProperMonthForm($months);
			}
		}
		
		$years = Carbon::now()->diffInYears($date, false);
		if($years < 0) {
			return self::getProperYearForm($years) ." temu";
		} else {
			return "za ". self::getProperYearForm($years);
		}

	}

}
