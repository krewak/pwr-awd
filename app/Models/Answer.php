<?php namespace App\Models;

class Answer {

	public $answer = "";
	public $icon = "";

	public function __construct($answer, $icon) {
		$this->answer = $answer;
		$this->icon = $icon;
	}

}
