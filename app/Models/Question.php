<?php namespace App\Models;

const SINGLE_CHOICE = "single";
const MULTIPLE_CHOICE = "multiple";
const OPEN_FIELD = "open";

class Question {

	protected $name = "";
	protected $question = "";
	protected $answers = [];
	protected $default_icon;
	protected $answers_limit = 1;
	protected $type;

	public function __construct($name) {
		$this->name = $name;
	}

	public function getQuestion() {
		return $this->question;
	}

	public function getAnswers() {
		return $this->answers;
	}

	public function getAnswersLimit() {
		return $this->answers_limit;
	}

	public function getName() {
		return $this->name;
	}

	public function getColumnClass() {
		$answers_count = count($this->getAnswers());

		$class = 3;
		if($answers_count == 2) {
			$class = 6;
		} elseif($answers_count%3 == 0) {
			$class = 4;
		}

		return "col-md-". $class ." col-sm-". $class;
	}

	public function isMultiple() {
		return $this->type == MULTIPLE_CHOICE;
	}

	public function isOpen() {
		return $this->type == OPEN_FIELD;
	}

	public function setQuestion($question) {
		$this->question = $question;
	}

	public function setAsSingleChoice() {
		$this->type = SINGLE_CHOICE;
	}

	public function setAsMultipleChoice($limit) {
		$this->type = MULTIPLE_CHOICE;
		$this->answers_limit = $limit;
	}

	public function setAsOpenField() {
		$this->type = OPEN_FIELD;
	}

	public function setAsBooleanChoice() {
		$this->type = SINGLE_CHOICE;
		$this->addAnswer("tak", "check");
		$this->addAnswer("nie", "close");
	}

	public function setAsSexChoice() {
		$this->type = SINGLE_CHOICE;
		$this->addAnswer("kobieta", "venus");
		$this->addAnswer("mężczyzna", "mars");
	}

	public function setDefaultIcon($icon) {
		$this->default_icon = $icon;
	}

	public function addAnswer($answer, $icon = null) {
		if(is_null($icon) && !is_null($this->default_icon)) {
			$icon = $this->default_icon;
		}
		array_push($this->answers, new Answer($answer, $icon));
	}

}
