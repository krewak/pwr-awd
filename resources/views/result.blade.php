@extends("master")

@section("content")

	<h1>Dziękujemy!</h1>
	<div class="question-area">

		<h1>Lista wyników</h1>
		<input type="hidden" id="_token" value="{{ csrf_token() }}">
		<input type="hidden" id="keywords" value="{{ $keywords }}">

		<div class="result text-center">
			{!! Html::image("images/preloader.gif", "Trwa ładowanie...", ["id" => "preloader", "data-checkfile" => route("checkfile"), "data-allegro" => route("allegro")]) !!}
		</div>

		<div id="result" class="result"></div>

	</div>

@endsection