@extends("master")

@section("content")
	<h1>Odpowiedz na pytania</h1>
	<form method="POST">
		<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
		<div class="question-area" data-count="{{ count($questions) }}">

			@foreach($questions as $index => $question)
			<div class="row text-center question @if($index) question-hidden @endif @if($question->isMultiple()) question-multiple @endif" data-qid="{{ $index + 1 }}" @if($question->isMultiple()) data-limit="{{ $question->getAnswersLimit() }}" @endif>
				<h2 class="question-title">{{ $question->getQuestion() }}</h2>
				@if(!$question->isOpen())
					<input class="answer" name="{{ $question->getName() }}" type="hidden">
					@foreach($question->getAnswers() as $answer)
					<div class="{{ $question->getColumnClass() }}">
						<div class="tile" data-value="{{ $answer->answer }}">
							<i class="fa fa-{{ $answer->icon }} fa-4x"></i><br>
							{{ $answer->answer }}
						</div>
					</div>
					@endforeach
				@else
					<div class="col-md-12 input-group">
						<span class="input-group-addon"><i class="fa fa-tags"></i></span>
						<input name="{{ $question->getName() }}" type="text" class="form-control" placeholder="Wpisz po przecinku słowa kluczowe, które Cię interesują">
					</div>
				@endif
			</div>
			@endforeach

			<div class="question-buttons">
				<div class="btn btn-warning previous-question hidden"><i class="fa fa-chevron-circle-left"></i> poprzednie pytanie</div>
				<div class="btn btn-warning next-question">nastepne pytanie <i class="fa fa-chevron-circle-right"></i></div>
				<input type="submit" class="btn btn-success submit-questions hidden" value="gotowe!">
			</div>
			<div class="questions-progress">
				<i class="fa fa-times-circle-o fa-spin" data-qid="1"></i>
				@foreach(range(2, count($questions)) as $step)
				<i class="fa fa-circle-o" data-qid="{{ $step }}"></i>
				@endforeach
			</div>
		</div>
	</form>
@endsection