<table class="table table-condensed small">
	<thead>
		<tr>
			<th>tytuł aukcji, stan, czas do końca</th>
			<th class="text-right">cena</th>
		</tr>
	</thead>
	<tbody>
		@foreach($items as $item)
		<tr>
			<td>
				<h3><a href="http://allegro.pl/show_item.php?item={{ $item->itemId }}" target="_blank">{{ $item->itemTitle }}</a></h3>
				<a href="http://allegro.pl/show_item.php?item={{ $item->itemId }}" target="_blank">stan: {{ $conditions[$item->conditionInfo] }}</a>,<br>
				<a href="http://allegro.pl/show_item.php?item={{ $item->itemId }}" target="_blank">{{ $item->timeToEnd }}</a>
			</td>
			<td class="text-right">
				@foreach($item->priceInfo->item as $price)
					{{ $price_types[$price->priceType] }}: {{ number_format($price->priceValue, 2) }}zł<br>
				@endforeach
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<h3 class="categories">Przeszukane kategorie</h3>
<div class="categories-list">
	@foreach($categories as $category)
		<a href="http://allegro.pl/kategoria-{{ $category->categoryId }}">{{ $category->categoryName }}</a>
	@endforeach
</div>