<!DOCTYPE html>
<html lang="pl">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="AWD">
	<meta name="author" content="Krzysztof Rewak">

	<title>AWD</title>

	{!! Html::style("css/vendor/bootstrap.min.css") !!}
	{!! Html::style("css/vendor/font-awesome.min.css") !!}
	{!! Html::style("css/style.css") !!}

	<link href="//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic&subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Oswald:400,700&subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
</head>
<body>

	<div id="wrapper">
		<div class="header">
			<div class="container">
				<div class="header-main">
					<div class="logo">
						<a href="{{ route("home") }}"><i class="fa fa-cart-plus"></i> <strong>AWD</strong>: System decyzyjny </a>
					</div>
					<div class="bottom">
						<div class="col-md-4 bottom-left">
							<h1>Jak to działa?</h1>
							<p class="paragraph">
								Aplikacja jest przeznacza dla osób, które poszukują prezentu dla bliskich i&nbsp;potrzebują pomocy w&nbsp;jego wyborze. Użytkownik zostanie poproszony o&nbsp;odpowiedź na kilka pytań, a&nbsp;zaszyty w&nbsp;trzewiach aplikacji system wspomagania decyzji (podłączony pośrednio do bazy danych Allegro) znajdzie najlepszą odpowiedź.
							</p>

							<h1>Technikalia</h1>
							<p class="paragraph">
								Witryna oraz załączony system decyzyjny powstały na potrzeby zajęć projektowych kursu <strong>AREU00602P Algorytmy wspomagania decyzji</strong> prowadzonym przez dr. inż. Andrzeja Rusieckiego na 2. semestrze stacjonarnych studiów II stopnia na Wydziale Elektroniki Politechniki Wrocławskiej.
							</p>
							<p class="paragraph">
								System decyzyjny został zaimplementowany w&nbsp;języku Prolog. Witryna powstała w&nbsp;oparciu o&nbsp;framework Laravel w&nbsp;języku PHP z&nbsp;wykorzystaniem JavaScriptu z&nbsp;jQuery.
							</p>
						</div>
						<div class="col-md-8 bottom-left">
							@yield("content")
						</div>
		  				<div class="clearfix"></div>
					</div>			
				</div>
			</div>
		</div>

		<div class="footer">
			<div class="container">
				<div class="footer-main">
					<div class="col-md-6 footer-left">
						<p class="footer-text">
							2015-16 &copy; <a href="http://rewak.eu/" target="_blank">Krzysztof Rewak</a> i <a href="http://bitbucket.org/mibalogh" target="_blank">Mihály Balogh</a>
						</p>
					</div>
					<div class="col-md-6 footer-right">
						<p class="footer-text">
							<a href="https://trello.com/b/56FHFBp5/algorytmy-wspomagania-decyzji-projekt" target="_blank"><i class="fa fa-trello fa-2x"></i></a>&nbsp;&nbsp;&nbsp;
							<a href="https://bitbucket.org/krewak/pwr-awd/" target="_blank"><i class="fa fa-bitbucket fa-2x"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	{!! Html::script("js/vendor/jquery.min.js") !!}
	{!! Html::script("js/vendor/bootstrap.min.js") !!}
	{!! Html::script("js/script.js") !!}

</body>
</html>